unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Menus, lresources;

const
  NUM_OF_UFOS = 50;
  NUM_OF_MY_LAZERS = 49;
  NUM_OF_UFOS_LAZERS = 120;
  EASY_START_VAL = 1000;


type
  WH = record
    //Ширина и длинна,вот и все.Название нормального не подобрать.
    Height, Width: integer;
  end;

  TCord = record     //корды всего,что только можно.
    x, y: integer;
  end;
  { UFO }
  UFO = class(TObject) //враги(НЛО)

    Cord1: TCord;
    WH1: WH;
    MyBitmap: TBitmap;
    NumOfUFOs: integer;
    allians: array[0..NUM_OF_UFOS] of TCord;
    UFOset: set of 0..NUM_OF_UFOS;
    constructor Create; overload;
    destructor Destroy; override;
    procedure NewUFO();
    procedure Shoot();//процедура вражеского выстрела.
    procedure NotMyMove(dx, dy: integer);//Рандомное движение НЛО
    procedure NotMyPaint(canvas: TCanvas);//Отрисовка НЛО
    procedure Delete(Ind: integer);

  end;

  { MC }
  MC = class(TObject)//мой корабль
    MyBitmap: TBitmap;
    Cord: TCord;
    WH1: WH;
    constructor Create; overload;
    destructor Destroy; override;
    procedure MyPaint(canvas: TCanvas);
    procedure MyMove(dx, dy: integer);
  end;

  { MyAtack }
  MyAtack = class(TObject)//мой лазер
    bullets: array[0..NUM_OF_MY_LAZERS] of TCord;
    procedure NewBullet(x, y: integer);
    procedure MyMove(dy: integer);
    procedure MyPaint(canvas: TCanvas);
    constructor Create; overload;
    destructor Destroy; override;
  end;

  { EnemyAtack }
  EnemyAtack = class(TObject)//лазер врага
    EnemyBullets: array[0..NUM_OF_UFOS_LAZERS] of TCord;
    procedure NewEnemyBullet(x, y: integer);
    procedure EnemyMove(dy: integer);
    procedure EnemyPaint(canvas: TCanvas);
    constructor Create; overload;
    destructor Destroy; override;

  end;

  { TForm1 }
  TForm1 = class(TForm)
    Timer1: TTimer;
    Timer2: TTimer;
    Timer3: TTimer;
    Timer4: TTimer;
    procedure Restart();
    procedure Timer4Timer(Sender: TObject);
    procedure WinOrLose();
    procedure Paint();overload;//отрисовка героев
    procedure FormActivate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure MyLazer();
    procedure EnemyLazer();
    procedure PaintBackGround();//отрисовка фона
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
  private

    mainCharLazers: MyAtack;// b
  public
    backgroundBMP: TBitmap;
    easy,easyCounter, easyDelta, points: integer;
    mainChar: MC;// a
    ufoLazers: EnemyAtack;// d
    ufoS: UFO;// c

    lifes: integer;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}


{ TForm1 }
procedure TForm1.FormActivate(Sender: TObject);
var
  i: integer;
  tmp: TBitmap;
begin
  randomize;
  backgroundBMP := TBitmap.Create;
  backgroundBMP.LoadFromLazarusResource('space');
  backgroundBMP.Transparent := True;
  backgroundBMP.TransparentColor := clWhite;


  tmp := TBitmap.Create;
  try
    tmp.Width := Self.Width;
    tmp.Height := Self.Height;
    tmp.Canvas.StretchDraw(Bounds(0,0,tmp.Width,tmp.Height), backgroundBMP);
    backgroundBMP.Assign(tmp);
  finally
    tmp.Free;
  end;
  easy := EASY_START_VAL;
  easyCounter:= 1;
  easyDelta:=33;
  lifes := 10;
  mainChar := MC.Create;  //созданю объект главного героя
  mainCharLazers := MyAtack.Create;  //создание пули ГГ
  ufoS := UFO.Create;   // создаю  НЛО
  ufoLazers := EnemyAtack.Create;  //создаю пули НЛО
end;

procedure TForm1.Restart();
begin

end;

{ TODO : реализовать провеку на проигрыш }
procedure TForm1.WinOrLose();
begin
  if lifes <= 0 then
  begin
  {case QuestionDlg ('Поражение','Вы проиграли.Хотите начать заново?',mtCustom,[mrYes,'Positive', mrNo, 'Negative', 'IsDefault'],'') of
        mrYes:);
        mrNo:  QuestionDlg ('Caption','Oh, you mean „No“',mtCustom,[mrOK,'Exactly'],'');
        mrCancel: QuestionDlg ('Caption','You canceled the dialog with ESC or close button.',mtCustom,[mrOK,'Exactly'],'');
    end;}
  end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  mainCharLazers.MyMove(-5);
  ufoLazers.EnemyMove(5);
  Self.MyLazer();
  Self.EnemyLazer();
  Paint;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
//по этой процедуре меняется корды НЛОшек и это-таймер,вызывающий стрельбу НЛОшек
var
  g: integer;
begin
  g := random(easy);
  if g < easy * 0.6 then
    ufoS.NewUFO();

  ufoS.NotMyMove(15, 10);
end;

procedure TForm1.Timer3Timer(Sender: TObject);
//таймер проверки очков жизни.Если их нет-игра проиграна.
var
  g: integer;
begin
  if lifes <= 0 then
    Form1.Close;
end;

procedure TForm1.Timer4Timer(Sender: TObject);
var
  g: integer;
begin
  ufoS.Shoot();
  //g := random(easy);
  //if g < easy * 0.9 then

end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);//нажатия клавиш
var
  dx, dy: integer;
begin
  dx := 0;
  dy := 0;
  case Key of
    37: dx := -5;
    38: dy := -5;
    39: dx := 5;
    40: dy := 5;
    70: mainCharLazers.NewBullet(mainChar.Cord.x, mainChar.Cord.y);
  end;//End of case ... of
  mainChar.MyMove(dx, dy);
end;//End of Procedure TForm1.FormKeyUp

procedure TForm1.MyLazer();//проверка попадания моей пули
var
  j, i: integer;
begin
  for i := 0 to NUM_OF_MY_LAZERS do
  begin
    if mainCharLazers.bullets[i].y = 0 then
      continue;
    for j := 0 to NUM_OF_UFOS do
    begin
      if (mainCharLazers.bullets[i].y < ufoS.allians[j].y + ufoS.WH1.Height) and
        (mainCharLazers.bullets[i].y > ufoS.allians[j].y) then
        if (mainCharLazers.bullets[i].x < ufoS.allians[j].x + ufoS.WH1.Width) and
          (mainCharLazers.bullets[i].x > ufoS.allians[j].x) then
        begin
          if easy >= 5 then
            Dec(easy,easyDelta);
          if easy < (EASY_START_VAL/easyCounter*0.6) then
          begin
             Dec(easyDelta, round(easyDelta*0.1));
             inc(easyCounter);
          end;
          Inc(points, 1);
          mainCharLazers.bullets[i].x := 0;
          mainCharLazers.bullets[i].y := 0;
          ufoS.Delete(j);
        end;
    end;
  end;
end;

procedure TForm1.EnemyLazer();   //проверка попадания вражеской пули
var

  i, j: integer;
begin
  for i := 0 to NUM_OF_UFOS_LAZERS do
  begin
    if ufoLazers.EnemyBullets[i].y = 0 then
      continue;
    if (ufoLazers.EnemyBullets[i].y < mainChar.Cord.y + mainChar.WH1.Height) and
      (ufoLazers.EnemyBullets[i].y > mainChar.Cord.y) then
      if (ufoLazers.EnemyBullets[i].x < mainChar.Cord.x + mainChar.WH1.Width) and
        (ufoLazers.EnemyBullets[i].x > mainChar.Cord.x) then
      begin

        ufoLazers.EnemyBullets[i].x := 0;
        ufoLazers.EnemyBullets[i].y := 0;
        Dec(points, 10);
        Dec(lifes);
        WinOrLose();
      end;
  end;
end;

procedure TForm1.PaintBackGround();//отрисовка фона
begin
  //with Self.Canvas do
  //begin
  //  Brush.Color := clblack;
  //  Pen.Color := clblack;
  //  rectangle(0, 0, Self.Width, Self.Height);
  //end;
  Self.Canvas.Draw(0,0, backgroundBMP);
end;

procedure TForm1.Paint();//главная отрисовка
begin
  inherited Paint;
  Self.PaintBackGround();
  mainChar.MyPaint(Self.canvas);
  mainCharLazers.MyPaint(Self.canvas);
  ufoS.NotMyPaint(Self.canvas);
  ufoLazers.EnemyPaint(Self.canvas);
  Self.Canvas.textout(0, 0, IntToStr(lifes));
  Self.Canvas.textout(0, 15, IntToStr(easy));
  Self.Canvas.textout(0, 30, IntToStr(points));
end;


{ MC }
constructor MC.Create;
begin
  inherited;
  MyBitmap := TBitmap.Create;
  Cord.x := 200;
  Cord.y := 350;
  WH1.Width := 30;
  WH1.Height := 30;
end;

destructor MC.Destroy;
begin
  inherited Destroy;
end;

procedure MC.MyMove(dx, dy: integer); //процедура движения моего корабля
begin
  if (Form1.Width > Cord.x + Self.WH1.Width + dx) and
    (Cord.x + Self.WH1.Width + dx > 0) then
    Cord.x := Cord.x + dx;
  if (Form1.Height > Cord.y + Self.WH1.Height + dy) and
    (Cord.y + Self.WH1.Height + dy > 0) then
    Cord.y := Cord.y + dy;
end;//End of procedure MC.Move

procedure MC.MyPaint(canvas: TCanvas);//отрисовка моего корабля
begin

  with canvas do
  begin
    Brush.Color := clred;
    Pen.Color := clred;
    Pen.Width := 3;
    rectangle(Cord.x, Cord.y, Cord.x + Self.WH1.Width, Cord.y + Self.WH1.Height);
  end;//End of With
end;//End of procedure MC.Paint


{ UFO }

constructor UFO.Create;
var

  buffer: THandle;
  memstream: TMemoryStream;
  i: integer;
begin
  inherited;
  MyBitmap := TBitmap.Create;
  WH1.Width := 27;
  WH1.Height := 15;
 {  for i := 0 to NUM_OF_UFOS do
  begin
    allians[i].x := 0;
    allians[i].y := 0;
  end;}
  NumOfUFOs := 0;
  MyBitmap.LoadFromLazarusResource('ufo');
  MyBitmap.Transparent := True;
  MyBitmap.TransparentColor := clWhite;

  //MyCanvas.Draw(0, 0, MyBitmap);

  //MyBitmap.Free; // Освобождаем выделенный ресурс
end;

destructor UFO.Destroy;

begin
  MyBitmap.Free;
  inherited Destroy;
end;

procedure UFO.NewUFO();
var
  i: integer;
begin
  for i := 1 to NUM_OF_UFOS do
    if allians[i].y = 0 then
    begin
      allians[i].y := random(Form1.Height div 2) + 1;
      allians[i].x := random(Form1.Width - 1) + 1;
      Inc(NumOfUFOs);
      include(UFOset, i);
      break;
    end;

end;

procedure UFO.NotMyMove(dx, dy: integer);//движение НЛО(врагов)
var
  dx1, dx1h, dy1, i: integer;
begin
  dx1 := dx;
  dx1h := dx div 2;
  dy1 := dy;
  for i := 0 to NUM_OF_UFOS do
  begin
    if allians[i].y = 0 then
      continue;
    dx := random(dx1) - dx1h;
    dy := random(dy1);
    if (Form1.Width > allians[i].x + Self.WH1.Width + dx) and
      (allians[i].x + Self.WH1.Width + dx > 0) then
      allians[i].x := allians[i].x + dx
    else
      allians[i].x := allians[i].x - dx;
    if (Form1.Height > allians[i].y + Self.WH1.Height + dy) and
      (allians[i].y + Self.WH1.Height + dy > 0) then
      allians[i].y := allians[i].y + dy
    else
    begin
      Dec(Form1.points, 10);
      Delete(i);
    end;
  end; //END OF FOR
end;//End of procedure UFO.NotMyMove

procedure UFO.NotMyPaint(canvas: TCanvas);//отрисовка НЛО
var
  i: integer;
begin
  with canvas do
  begin
    //Pen.Color := clblack;
    //Pen.Width := 3;
    for i := 0 to NUM_OF_UFOS do
    begin
      if allians[i].y = 0 then
        continue;
      Draw(allians[i].x, allians[i].y, MyBitmap);
      //rectangle(allians[i].x, allians[i].y, allians[i].x + Self.WH1.Width,
      //allians[i].y + Self.WH1.Height);
    end;
  end;//End of With
end;//End of procedure UFO.NotMyPaint

procedure UFO.Shoot(); //стрельба НЛО
var
  i, g: integer;
begin
  randomize;
  for i := 0 to NUM_OF_UFOS do
  begin
    g := random(NUM_OF_UFOS);
    if g in UFOset then
    begin
      Form1.ufoLazers.NewEnemyBullet(allians[g].x + WH1.Width div 2,
        allians[g].y + WH1.Height);
      break;
    end
  end;
end;

procedure UFO.Delete(Ind: integer);
begin
  if Ind in UFOset then
  begin
    exclude(UFOset, Ind);
    Dec(NumOfUFOs);
    allians[Ind].y := 0;
    allians[Ind].x := 0;
  end;
end;


{ MyAtack }
constructor MyAtack.Create;
var
  i: integer;
begin
  inherited;
  for i := 0 to NUM_OF_MY_LAZERS do
  begin
    Bullets[i].x := 0;
    Bullets[i].y := 0;
  end;

end;

destructor MyAtack.Destroy;
begin
  inherited Destroy;
end;

procedure MyAtack.MyPaint(canvas: TCanvas);
//отрисовка моего выстрела
var
  i: integer;
begin
  for i := 0 to NUM_OF_MY_LAZERS do
  begin
    if bullets[i].y <> 0 then

      with canvas do
      begin

        Pen.Color := clred;
        Pen.Width := 3;
        Line(bullets[i].x, bullets[i].y, bullets[i].x, bullets[i].y);

      end;//End of With
  end;

end;//End of Procedure MyAtack.Paint

procedure MyAtack.NewBullet(x, y: integer); //создание моей пули(берем свободную(корды 0;0))
var
  i: integer;
begin
  for i := 0 to NUM_OF_MY_LAZERS do
  begin

    if bullets[i].x = 0 then
    begin
      bullets[i].x := x + 5;
      bullets[i].y := y + 5;
      break;
    end;
  end;

end;

procedure MyAtack.MyMove(dy: integer);//движение моей пули
var
  i: integer;
begin
  for i := 0 to NUM_OF_MY_LAZERS do
  begin

    if (bullets[i].y > 0) and (bullets[i].y <> 0) then
      bullets[i].y := bullets[i].y + dy
    else
    begin

      bullets[i].y := 0;
      bullets[i].x := 0;

    end;
  end;
end;


{ EnemyAtack }
constructor EnemyAtack.Create;
var
  i: integer;
begin
  inherited;
  for i := 0 to NUM_OF_UFOS_LAZERS do
  begin
    EnemyBullets[i].x := 0;
    EnemyBullets[i].y := 0;
  end;
end;

destructor EnemyAtack.Destroy;
begin
  inherited Destroy;
end;

procedure EnemyAtack.NewEnemyBullet(x, y: integer);
//создаие вражеской пули
var
  i: integer;
begin
  for i := 0 to NUM_OF_UFOS_LAZERS do
  begin

    if EnemyBullets[i].x = 0 then
    begin

      EnemyBullets[i].x := x;
      EnemyBullets[i].y := y;
      break;
    end;
  end;

end;

procedure EnemyAtack.EnemyMove(dy: integer);//движение вражеской пули
var
  i: integer;
begin
  for i := 0 to NUM_OF_UFOS_LAZERS do
  begin
    if (EnemyBullets[i].y <= Form1.Height) and (EnemyBullets[i].y <> 0) then
      EnemyBullets[i].y := EnemyBullets[i].y + dy
    else
    begin
      EnemyBullets[i].y := 0;
      EnemyBullets[i].x := 0;
    end;
  end;
end;

procedure EnemyAtack.EnemyPaint(canvas: TCanvas);
//отрисовка вражеской пули
var
  i: integer;
begin

  for i := 0 to NUM_OF_UFOS_LAZERS do
  begin

    if EnemyBullets[i].y <> 0 then

      with canvas do
      begin

        Pen.Color := clred;
        Pen.Width := 3;
        Line(EnemyBullets[i].x, EnemyBullets[i].y, EnemyBullets[i].x, EnemyBullets[i].y);

      end;//End of With
  end;

end;


initialization
  {$I killTheUFO.lrs}
end.
